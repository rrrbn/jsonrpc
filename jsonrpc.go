package main

import (
	"io"
	"net/rpc"
	"net/rpc/jsonrpc"
	"net/http"
	"code.google.com/p/go.net/websocket"
	"log"
	"html/template"
)

var tmpl = template.Must(template.ParseFiles("jsonrpc.html"))

// Args are arguments for Tests Add function.
type Args struct {
	A	int
	B	int
}

// Test is an example object exported by rpc
type Test struct {
}

// Add is exported by the rpc package
func (t *Test) Add(args Args, result *int) error {
	*result = args.A + args.B
	return nil
}

// readWriteCloser implements io.ReadWriteCloser interface
// which jsonrpc.NewServerCodec wants. Close is a nop function
// as the http package handles closing of the body.
type readWriteCloser struct {
	io.Reader
	io.Writer
}

func (r readWriteCloser) Close() error {
	return nil
}

// NewReadWriteCloser returns a new object implementing
// io.ReadWriteCloser. To use it with jsonrpc call it with
// the Body of the request as reader and the ResponseWriter
// as writer.
func NewReadWriteCloser(r io.Reader, w io.Writer) readWriteCloser {
	return readWriteCloser{r, w}
}

func main() {
	t := new(Test)
	s := rpc.NewServer()
	if err := s.Register(t); err != nil {
		log.Fatal(err)
	}

	http.HandleFunc("/rpc", func(w http.ResponseWriter, r *http.Request) {
		rw := NewReadWriteCloser(r.Body, w)

		log.Println(s.ServeRequest(jsonrpc.NewServerCodec(rw)))

		})

	http.Handle("/wsrpc", websocket.Handler(func(ws *websocket.Conn) {
		s.ServeCodec(jsonrpc.NewServerCodec(ws))
		}))

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		tmpl.Execute(w, nil)
	})

	http.Handle("/res/", http.StripPrefix("/res/", http.FileServer(http.Dir("."))))

	log.Fatal(http.ListenAndServe(":8080",nil))
}



