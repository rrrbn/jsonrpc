var schemaWebsocket = function(url) {
	if (url.slice(0,5) == "ws://" || url.slice(0,6) == "wss://") {
		return true
	}

	return false
}

var rpc = function rpc(url) {
	this.constructor;
	this.url = url;
	this.id = 0;

	if (schemaWebsocket(url)) {
		requests = new Map();
		ws = new WebSocket(this.url);
		ws.onopen = function(event) { 
			ws.onmessage = function(event) {
				var response = JSON.parse(event.data);
				if (response.error == null) {
					requests.get(response.id).success(response.result);
				} else {
					requests.get(response.id).error(response.error);
				}
			}
		};

		this.requests = requests;
		this.ws = ws;
	}
}

rpc.prototype = {
	call: function call(method, params, success, error) {
		if (this.ws != null) {
			var request = {
				"method": method,
				"params": params,
				"id": this.id
			}

			this.ws.send(JSON.stringify(request));
			this.requests.set(this.id, {"success": success, "error": error});
			this.id++;
		} else {
			var xhttp = new XMLHttpRequest();
			xhttp.onreadystatechange = function () {
				if (xhttp.readyState == 4 && xhttp.status == 200) {
					var jsonData = JSON.parse(xhttp.responseText);
					if (jsonData.error == null) {
						success(jsonData.result);
					} else {
						error(jsonData.error);
					}
				}
			}

			var request = {
				"method": method,
				"params": params,
				"id": this.id++
			}

			xhttp.open("POST", this.url, true);
			xhttp.setRequestHeader("Content-Type", "application/json");
			xhttp.send(JSON.stringify(request));
		}
	}
}
